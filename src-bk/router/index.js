import { createRouter, createWebHistory } from 'vue-router'
import Signin from "@/views/Signin";
import Dashboard from "@/views/home/Dashboard";
import Profile from "@/views/users/Profile";
import Addrole from "@/views/roles/Addrole";
import Addcategories from "@/views/categories/Addcategories";
import Roles from "@/views/roles/Roles";
import Editrole from "@/views/roles/Editrole";
import Users from "@/views/users/Users";
import Edituser from "@/views/users/Edituser";
import Adduser from "@/views/users/Adduser";
import Categories from "@/views/categories/Categories";
import Editcategories from "@/views/categories/Editcategories";
const routes = [
  {
    path: "/dashboard",
    component: Dashboard,
    name: "Dashboard",
  },

  {
    path: "/my-profile",
    component: Profile,
    name: "Profile",
  },
  {
    path: "/all-categories",
    component: Categories,
    name: "Categories",
  },
  {
    path: "/all-categories/:id",
    component: Categories,
    name: "Categorieswithid",
  },
  {
    path: "/create-categories",
    component: Addcategories,
    name: "Addcategories",
  },
  {
    path: "/edit-category/:id",
    component: Editcategories,
    name: "Editcategories",
  },
  {
    path: "/create-role",
    component: Addrole,
    name: "Addrole",
  },
  {
    path: "/roles",
    component: Roles,
    name: "Roles",
  },
  {
    path: "/edit-role/:id",
    component: Editrole,
    name: "Editrole",
  },
  {
    path: "/all-users",
    component: Users,
    name: "Users",
  },
  {
    path: '/edit-user/:id/:roleId',
    name: 'Edituser',
    component: Edituser
  },

  {
    path: "/create-user",
    component: Adduser,
    name: "Adduser",
  },
  {
    path: "/",
    component: Signin,
    name: "Signin",
    meta: { bodyId: 'register_bg' }
  },

];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
