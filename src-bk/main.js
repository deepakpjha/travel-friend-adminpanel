import { createApp } from 'vue'
import App from './App.vue'
import router from './router.js'
import store from './store'
import "bootstrap"
import "./assets/vendor/bootstrap/css/bootstrap.min.css"
import { FontAwesomeIcon } from './plugins/font-awesome'
import "jquery"

import "./assets/css/admin.css"
import "./assets/vendor/font-awesome/css/font-awesome.min.css"
import "./assets/vendor/datatables/dataTables.bootstrap4.css"
import "./assets/vendor/dropzone.css"
import "./assets/css/custom.css"

createApp(App)
    .use(store)
    .use(router)
    .component("font-awesome-icon", FontAwesomeIcon)
    .mount('#app')
