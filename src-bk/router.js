import { createRouter, createWebHistory } from 'vue-router'
import Signin from "@/views/Signin";

const Dashboard = () => import("./views/home/Dashboard.vue")
const Profile = () => import("./views/users/Profile.vue")
const Users = () => import("./views/users/Users.vue")
const Adduser = () => import("./views/users/Adduser.vue")
const Edituser = () => import("./views/users/Edituser.vue")
const Roles = () => import("./views/roles/Roles.vue")
const Addrole = () => import("./views/roles/Addrole.vue")
const Editrole = () => import("./views/roles/Editrole.vue")
const Categories = () => import("./views/categories/Categories.vue")
const Addcategories = () => import("./views/categories/Addcategories.vue")
const Editcategories = () => import("./views/categories/Editcategories.vue")


const routes = [
  {
    path: "/dashboard",
    component: Dashboard,
    name: "Dashboard",
  },

  {
    path: "/my-profile",
    component: Profile,
    name: "Profile",
  },
  {
    path: "/all-categories",
    component: Categories,
    name: "Categories",
  },
  {
    path: "/all-categories/:id",
    component: Categories,
    name: "Categorieswithid",
  },
  {
    path: "/create-categories",
    component: Addcategories,
    name: "Addcategories",
  },
  {
    path: "/edit-category/:id",
    component: Editcategories,
    name: "Editcategories",
  },
  {
    path: "/create-role",
    component: Addrole,
    name: "Addrole",
  },
  {
    path: "/roles",
    component: Roles,
    name: "Roles",
  },
  {
    path: "/edit-role/:id",
    component: Editrole,
    name: "Editrole",
  },
  {
    path: "/all-users",
    component: Users,
    name: "Users",
  },
  {
    path: '/edit-user/:id/:roleId',
    name: 'Edituser',
    component: Edituser
  },

  {
    path: "/create-user",
    component: Adduser,
    name: "Adduser",
  },
  {
    path: "/",
    component: Signin,
    name: "Signin",
    meta: { bodyId: 'register_bg' }
  },

];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('token');
  const roleId = localStorage.getItem('role_id');

  // trying to access a restricted page + not logged in
  // redirect to login page
  if (authRequired && (!loggedIn || !roleId)) {
    next('/');
  } else {
    next();
  }
});
export default router
