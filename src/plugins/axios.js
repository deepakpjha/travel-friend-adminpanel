import axios from 'axios';
import router from '../router';
const API_URL = 'https://peekzy.in/backend/public/';
/*const guest = axios.create({
  baseURL: API_URL,
  //baseURL: "http://localhost/8080/",
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
    "X-Requested-With": "XMLHttpRequest",
  },
});*/

const headers = {
  'Content-Type': 'application/json',
  "X-Requested-With": "XMLHttpRequest",
}

const header2 = {
  username: 'testclient',
  password: 'testsecret',
  grant_type: "password",
  'Content-Type': 'application/json',
  "X-Requested-With": "XMLHttpRequest",
}
const api = axios.create({
  baseURL: API_URL,
  //baseURL: "http://localhost/8080/",
  headers: headers,
});

api.interceptors.request.use((config) => {
  const token = localStorage.getItem("token");
  if (token) {
    config.headers["Authorization"] = "Bearer " + token;
  }

  return config;
});

class AuthService {
  async login(loginData) {
    const response = await axios
      .post(API_URL + 'signin', loginData, header2);
    if (response.data.access_token) {
      const expiresMs = this.expires_in * 1000;
      const now = new Date();
      const expireDate = new Date(now.getTime() + expiresMs);
      localStorage.setItem('token', JSON.stringify(response.data.access_token));
      localStorage.setItem("role_id", response.data.role_id);
      localStorage.setItem("expires", expireDate);

      this.$store.dispatch("login", expiresMs);
      // router.push("/my-account");
      // alert(this.role_id);
      if (response.data.role_id == 1) {
        // alert('hi');
        router.push("/dashboard");
      } else {
        // alert('hello');
        router.push("/");
      }
    } else {
      this.$emit('message')
    }
    return response.data;
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('role_id');
  }

  register(user) {
    return axios.post(API_URL + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password
    }, {
      auth: {
        username: "testclient",
        password: "testsecret",
      },
    });
  }
}

export default new AuthService();
