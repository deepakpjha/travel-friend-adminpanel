import axios from "axios";

export default axios.create({
  baseURL: "https://peekzy.in/backend/public/",
  // baseURL: `http://localhost:8080/`,
})