import { createStore } from 'vuex'
import { auth } from './auth.module'

const store = createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth
  }
})

export default store;
